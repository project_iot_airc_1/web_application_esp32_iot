/* WiFi station Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/*update 11 / 10 / 2023 ---- fomat: day/month/years*/
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"

#include <esp_http_server.h>
#include "dht11.h"
#include "HTTP_Server_app.h"
#include "ledc_app.h"
#define TAG1 "SENSOR"
/* The examples use WiFi configuration that you can set via project configuration menu

   If you'd rather not, just change the below entries to strings with
   the config you want - ie #define EXAMPLE_WIFI_SSID "mywifissid"
*/
#define EXAMPLE_ESP_WIFI_SSID      "."
#define EXAMPLE_ESP_WIFI_PASS      "12345688"
// #define EXAMPLE_ESP_WIFI_SSID      "P614-new"
// #define EXAMPLE_ESP_WIFI_PASS      "vcntt2017"
// #define EXAMPLE_ESP_WIFI_SSID      "GW020-H_2.4G_CC3998"
// #define EXAMPLE_ESP_WIFI_PASS      "0981231344"
#define EXAMPLE_ESP_MAXIMUM_RETRY  5



/* FreeRTOS event group to signal when we are connected*/
static EventGroupHandle_t s_wifi_event_group;
/* The event group allows multiple bits for each event, but we only care about two events:
 * - we are connected to the AP with an IP
 * - we failed to connect after the maximum amount of retries */
#define WIFI_CONNECTED_BIT BIT0
#define WIFI_FAIL_BIT      BIT1


int retry_connect_wifi = 1;

static const char *TAG = "wifi station";

static int s_retry_num = 0;

static struct dht11_reading dht11_last_data, dht11_current_data;

static void event_handler(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI(TAG, "retry to connect to the AP");
        } else {
            xEventGroupSetBits(s_wifi_event_group, WIFI_FAIL_BIT);
        }
        ESP_LOGI(TAG,"connect to the AP fail");
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI(TAG, "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        s_retry_num = 0;
        xEventGroupSetBits(s_wifi_event_group, WIFI_CONNECTED_BIT);
    }
}


void wifi_init_sta(void)
{
    s_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_netif_init());

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    esp_netif_create_default_wifi_sta();

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));

  //  esp_event_handler_instance_t instance_any_id;
   // esp_event_handler_instance_t instance_got_ip;
   // ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
   //                                                     ESP_EVENT_ANY_ID,
   //                                                     &event_handler,
   //                                                     NULL,
    //                                                    NULL));
   // ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
   //                                                     IP_EVENT_STA_GOT_IP,
   //                                                     &event_handler,
   //                                                     NULL,
    //                                                    NULL));
    
   ESP_ERROR_CHECK( esp_event_handler_register(WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL) );
   ESP_ERROR_CHECK(esp_event_handler_register(IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL));


    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS,
            /* Setting a password implies station will connect to all security modes including WEP/WPA.
             * However these modes are deprecated and not advisable to be used. Incase your Access point
             * doesn't support WPA2, these mode can be enabled by commenting below line */
	     .threshold.authmode = WIFI_AUTH_WPA2_PSK,
        },
    };
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

   // ESP_LOGI(TAG, "wifi_init_sta finished.");

    /* Waiting until either the connection is established (WIFI_CONNECTED_BIT) or connection failed for the maximum
     * number of re-tries (WIFI_FAIL_BIT). The bits are set by event_handler() (see above) */
    EventBits_t bits = xEventGroupWaitBits(s_wifi_event_group,
            WIFI_CONNECTED_BIT | WIFI_FAIL_BIT,
            pdFALSE,
            pdFALSE,
            portMAX_DELAY);

    /* xEventGroupWaitBits() returns the bits before the call returned, hence we can test which event actually
     * happened. */
    if (bits & WIFI_CONNECTED_BIT) {
        ESP_LOGI(TAG, "connected to ap SSID:%s password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } else if (bits & WIFI_FAIL_BIT) {
        ESP_LOGI(TAG, "Failed to connect to SSID:%s, password:%s",
                 EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
    } /*else {
        ESP_LOGE(TAG, "UNEXPECTED EVENT");
    }*/

    /* The event will not be processed after unregister */
   // ESP_ERROR_CHECK(esp_event_handler_instance_unregister(IP_EVENT, IP_EVENT_STA_GOT_IP, instance_got_ip));
   // ESP_ERROR_CHECK(esp_event_handler_instance_unregister(WIFI_EVENT, ESP_EVENT_ANY_ID, instance_any_id));
  //  vEventGroupDelete(s_wifi_event_group);
}
uint8_t check_data;
uint8_t check_duty =0;
void slider_range_data_callback(char *data, int len)
{
    char number_str[10];
        memcpy(number_str, data, len + 1); 
        int duty = atoi(number_str); //chuyen chuoi  ky tu thanh chuoi int
        check_duty= duty;
    if ( check_data == 1)
    {
        ledc_set_duty_io(0, check_duty);
    }
        
}

void switch_data_callback(char *data, int len)
{
    // gpio_pad_select_gpio(GPIO_NUM_2);
    // gpio_set_direction(GPIO_NUM_2, GPIO_MODE_OUTPUT);
    if (*data  == '1')
    {
        check_data=1;
        ledc_set_duty_io(0, check_duty);
    }
    if (*data  == '0')
    {
        check_data=0;
        ledc_set_duty_io(0, 1);
    }
}
void dht11_data_callback(void)
{
    char resp[100];
    int tempee;
    int tempeefloat;

    int humii;
    int humiifloat;
    if (dht11_last_data.temperature != 0)
    {
        tempee = dht11_last_data.temperature;
        tempeefloat = dht11_last_data.temperaturefloat;
        humii = dht11_last_data.humidity ;
        humiifloat = dht11_last_data.humidityfloat;
    }
    if (dht11_last_data.temperature == 0 )
    {
        tempee = 27;
        tempeefloat = 2;
        humii = 65;
        humiifloat = 5;
    }
    sprintf(resp, "{\"temperature\" : \"%d.%d\", \"humidity\": \"%d.%d\"}", tempee,tempeefloat , humii, humiifloat);
    dht11_response(resp, strlen(resp));
}

void app_main(void)
{
    //Initialize NVS
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);
    
    http_set_callback_switch(switch_data_callback);
    http_set_callbakc_dht11(dht11_data_callback);
    http_set_callback_slider_range(slider_range_data_callback);

    DHT11_init(GPIO_NUM_13);

    ledc_init();
    ledc_add_pin(GPIO_NUM_2, 0);

    ESP_LOGI(TAG, "ESP_WIFI_MODE_STA");
    wifi_init_sta();
    start_webserver();
    
    while (1){
        dht11_current_data= DHT11_read();
        if (dht11_current_data.status == 0)//read ok
        {
            dht11_last_data = dht11_current_data;
            // printf("0k\n");
        }
        vTaskDelay(2000/portTICK_RATE_MS);
    }
}