/* Simple HTTP Server Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>
#include <esp_system.h>
// #include <nvs_flash.h>
#include <sys/param.h>
// #include "nvs_flash.h"
#include "esp_netif.h"
#include "esp_eth.h"

#include <esp_http_server.h>
#include "HTTP_Server_app.h"

// #define TAG1 "SENSOR"
/* A simple example that demonstrates how to create GET and POST
 * handlers for the web server.
 */

static const char *TAG = "HTTP_Server";
static httpd_handle_t server = NULL;
static httpd_req_t *REQ;

extern const uint8_t index_html_start[] asm("_binary_index_html_start"); 
extern const uint8_t index_html_end[] asm("_binary_index_html_end"); 

static http_post_callback_t http_post_switch_callback = NULL;
static http_post_callback_t http_post_slider_range_callback = NULL;

static http_get_callback_t http_get_dht11_callback = NULL;


/* An HTTP GET handler */
static esp_err_t hello_get_handler(httpd_req_t *req)
{
    //const char* resp_str = (const char*) "data_dht11";
    httpd_resp_set_type(req, "text/html");

    httpd_resp_send(req, (const char *)index_html_start, index_html_end-index_html_start);
    
    /* After sending the HTTP response the old HTTP request
     * headers are lost. Check if HTTP request headers can be read now. */
    return ESP_OK;
}

static const httpd_uri_t get_dht11 = {
    .uri       = "/dht11",
    .method    = HTTP_GET,
    .handler   = hello_get_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};
/*-------------------------------------------------*/


/* An HTTP GET handler */


void dht11_response(char *data, int len)
{
    httpd_resp_send(REQ, data, len );

}


static esp_err_t get_data_dht11_handler(httpd_req_t *req)
{
    // char res[100]= "";
    
    
    // // const char* res = (const char*) "{\"temperature\" : \"50\", \"humidity\": \"80\"}";
    // sprintf(res,"{\"temperature\": \"%.2f\", \"humidity\": \"%.2f\"}",tempe,humi);
    
    // //httpd_resp_send(req, (const char *)index_html_start, index_html_end-index_html_start);
    // // httpd_resp_send(req, resp_str, strlen(resp_str) );
    // httpd_resp_send(req, res, strlen(res) );
    
    // /* After sending the HTTP response the old HTTP request
    //  * headers are lost. Check if HTTP request headers can be read now. */
    REQ = req; 
    http_get_dht11_callback();
    return ESP_OK;
}

static const httpd_uri_t get_data_dht11 = {
    .uri       = "/getdatadht11",
    .method    = HTTP_GET,
    .handler   = get_data_dht11_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};
/*--------------------------------------------------*/


//----------------------------------------------------
/*-------------------POST----------------------------*/

/* An HTTP POST handler */
static esp_err_t data_post_handler(httpd_req_t *req)
{
    char buf[100];
        /* Read the data for the request */
    httpd_req_recv(req, buf,req->content_len);
    printf("data: %s\n\n", buf);
    

    // End response
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;

}

static const httpd_uri_t post_data_dht11 = {
    .uri       = "/data",
    .method    = HTTP_POST,
    .handler   = data_post_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};

/*---------------slider_range-------------------*/
static esp_err_t slider_range_post_handler(httpd_req_t *req)
{
    char buf[100];
        /* Read the data for the request */
    httpd_req_recv(req, buf,req->content_len);
    http_post_slider_range_callback (buf, req->content_len);
    // End response
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;

}

static const httpd_uri_t slider_range_post_data = {
    .uri       = "/slider",
    .method    = HTTP_POST,
    .handler   = slider_range_post_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};




/*----------------------------------------------*/
/*---------------switch post-------------------*/
static esp_err_t sw_post_handler(httpd_req_t *req)
{
    char buf[100];
        /* Read the data for the request */
    httpd_req_recv(req, buf,req->content_len);
    http_post_switch_callback (buf, req->content_len);
    // End response
    httpd_resp_send_chunk(req, NULL, 0);
    return ESP_OK;

}

static const httpd_uri_t switch1_post_data = {
    .uri       = "/sw1",
    .method    = HTTP_POST,
    .handler   = sw_post_handler,
    /* Let's pass response string in user
     * context to demonstrate it's usage */
    .user_ctx  = NULL
};
/*---------------------------------------------*/
/* This handler allows the custom error handling functionality to be
 * tested from client side. For that, when a PUT request 0 is sent to
 * URI /ctrl, the /hello and /echo URIs are unregistered and following
 * custom error handler http_404_error_handler() is registered.
 * Afterwards, when /hello or /echo is requested, this custom error
 * handler is invoked which, after sending an error message to client,
 * either closes the underlying socket (when requested URI is /echo)
 * or keeps it open (when requested URI is /hello). This allows the
 * client to infer if the custom error handler is functioning as expected
 * by observing the socket state.
 */
esp_err_t http_404_error_handler(httpd_req_t *req, httpd_err_code_t err)
{
    if (strcmp("/hello", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/hello URI is not available");
        /* Return ESP_OK to keep underlying socket open */
        return ESP_OK;
    } else if (strcmp("/echo", req->uri) == 0) {
        httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "/echo URI is not available");
        /* Return ESP_FAIL to close underlying socket */
        return ESP_FAIL;
    }
    /* For any other URI send 404 and close socket */
    httpd_resp_send_err(req, HTTPD_404_NOT_FOUND, "Some 404 error message");
    return ESP_FAIL;
}

void start_webserver(void)
{
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.lru_purge_enable = true;

    // Start the httpd server
    ESP_LOGI(TAG, "Starting server on port: '%d'", config.server_port);
    if (httpd_start(&server, &config) == ESP_OK) {
        // Set URI handlers
        ESP_LOGI(TAG, "Registering URI handlers");
        httpd_register_uri_handler(server, &post_data_dht11);
        httpd_register_uri_handler(server, &get_data_dht11);
        httpd_register_uri_handler(server, &get_dht11);
        httpd_register_uri_handler(server, &slider_range_post_data);
        httpd_register_uri_handler(server, &switch1_post_data);
        


        httpd_register_err_handler(server, HTTPD_404_NOT_FOUND , http_404_error_handler);
        
    }
    else{
        ESP_LOGI(TAG, "Error starting server!");
    }
    
}

 void stop_webserver(void)
{
    // Stop the httpd server
    httpd_stop(server);
}


void http_set_callback_switch(void *cb)
{

    http_post_switch_callback = cb;
}

void http_set_callback_slider_range(void *cb)
{

    http_post_slider_range_callback = cb;
}


void http_set_callbakc_dht11(void *cb)
{
    http_get_dht11_callback = cb;
}

