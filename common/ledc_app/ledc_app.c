/* LEDC (LED Controller) fade example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/ledc.h"
#include "esp_err.h"

void ledc_init(void)
{
    /*
     * Prepare and set configuration of timers
     * that will be used by LED Controller
     */
    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_13_BIT, // resolution of PWM duty
        .freq_hz = 5000,                      // frequency of PWM signal
        .speed_mode = LEDC_HIGH_SPEED_MODE,           // timer mode
        .timer_num = LEDC_TIMER_1,            // timer index
        .clk_cfg = LEDC_AUTO_CLK,              // Auto select the source clock
    };
    ledc_timer_config(&ledc_timer);
}


void ledc_add_pin(int pin, int channel)
{
    ledc_channel_config_t ledc_channel = 
    {

            .channel    = channel,
            .duty       = 0,
            .gpio_num   = pin,
            .speed_mode = LEDC_HIGH_SPEED_MODE,
            .hpoint     = 0,
            .timer_sel  = LEDC_TIMER_1
    };
    
    ledc_channel_config(&ledc_channel);

}
/*duty range: 0 to 100*/
void ledc_set_duty_io(int channel, int duty)
{
    ledc_set_duty(LEDC_HIGH_SPEED_MODE, channel, duty*81);
    ledc_update_duty(LEDC_HIGH_SPEED_MODE, channel);

}
